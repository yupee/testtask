<?php 
include './config.php';
include './Autoloader.php';

Autoloader::init('./Bussiness');
Autoloader::init('./Web/Controllers');
Autoloader::init('./Web/Tools');
Autoloader::init('./Web/Interfaces');
spl_autoload_register('Autoloader::load');


$storage = SingletonMySqlDb::GetInstance();
$factory = new Factory();
$userRepository = new Repository($storage, 'User', $factory);
$userService = new UserService($userRepository, $factory);

$phonbookRepository = new Repository($storage, 'Phonebook', $factory);
$phonebookService = new PhonebookService($phonbookRepository, $factory);

$dispatcher = SingletonDispatcher::GetInstance();
$sanitizer = new Sanitizer();
$request = new Request($sanitizer);

$validator = new InputValidator();
$session = new SessionStorage();
$dispatcher->dispatch($request, $phonebookService, $userService, $session, $validator);

