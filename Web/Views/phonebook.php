<div class="modal fade" tabindex="-1" role="dialog" id="modal-wnd">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="savebtn">Сохранить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">Телефонная книги пользователя <a href="<?= $data['addLink'] ?>" class="ajax" id="createPhonebook"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div>

    </div>
    <?php if (count($data['phonebooks']) > 0) { ?>
    <ul class="phonebooks nav nav-tabs row">
        
        <?php 
        $class="active";
        foreach ($data['phonebooks'] as $phonebook) { ?>
            <li  role="presentation" class="<?php echo($class); $class=""; ?>  col-xs-6 col-sm-4 col-md-3">
                <a class="phonebook" aria-controls="phonebook<?= $phonebook['id'] ?>" role="tab" data-toggle="tab" href="#phonebook<?= $phonebook['id'] ?>">
                    <span class="h4"><?= $phonebook['name'] ?></span>
                    <span class="text">Количество контаков <?= count($phonebook['contacts']) ?></span>
                </a>
            </li>

        
    
    <?php } ?>
    </ul>    
        <?php } else {?>
            <div class="row">
                <div class="col-xs-12">
                    <p class="bg-warning">У вас нет ни одной телефонной книги</p>
                </div>
            </div>
        <?php } ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="tab-content">
            <?php 
            $class="active"; 
            foreach ($data['phonebooks'] as $phonebook) { ?>
                <div role="tabpanel" class="tab-pane <?php echo($class); $class=""; ?>" id="phonebook<?= $phonebook['id'] ?>">
                    <?php
                        $data['contacts'] = $phonebook['contacts'];
                        $data['addContactLink'] = $phonebook['addContactLink'];
                        include($data['contactsPath']);
                    ?>


                </div>
            <?php } ?>
            </div>
        </div>

    </div>
</div>