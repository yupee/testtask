<?php foreach ($data['contacts'] as $contact) { ?>
                    <tr>
                        <td><img src="<?= $contact['image'] ?>" alt="<?= $contact['name'] ?>"></td>
                        <td><?= $contact['name'] ?></td>
                        <td><?= $contact['surname'] ?></td>
                        <td><?= $contact['company'] ?></td>
                        <td><?= $contact['position'] ?></td>
                        <td><?= $contact['email'] ?></td>
                        <td><?= $contact['phone1'] ?></td>
                        <td><?= $contact['phone2'] ?></td>
                        <td>
                            <a class="ajax viewContact" href="<?= $contact['viewLink'] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                            <a class="ajax editContact" href="<?= $contact['editLink'] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <a class="ajax removeContact" href="<?= $contact['removeLink'] ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </td>
                    </tr>
                <?php } ?>