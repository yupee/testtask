<?php $contact = $data['contact']; ?>
<form method="post" action="<?= $data['saveLink'] ?>" enctype="multipart/form-data" class="row add-contact-form">    
    <div class="col-xs-12 ">
        <label>Аватарка</label>
        <input type="file" name="image" accept="image/jpeg,image/png,image/gif"></div>
    <div class="col-xs-12 ">
        <label>Имя</label>
        <input type="text" value="<?= $contact['name'] ?>" name="contact[name]" id="name"></div>
    <div class="col-xs-12 ">
        <label>Фамилия</label>
        <input type="text" value="<?= $contact['surname'] ?>" name="contact[surname]" id="surname"></div>
    <div class="col-xs-12 ">
        <label>Компания</label>
        <input type="text" value="<?= $contact['company'] ?>" name="contact[company]" id="company"></div>
    <div class="col-xs-12 ">
        <label>Должность</label>
        <input type="text" value="<?= $contact['position'] ?>" name="contact[position]" id="position"></div>
    <div class="col-xs-12 ">
        <label>Почта</label>
        <input type="email" value="<?= $contact['email'] ?>" name="contact[email_email]" id="email"></div>
    <div class="col-xs-12 ">
        <label>Мобильный телефон</label>
        +7<input type="text" value="<?= $contact['phone1'] ?>" name="contact[phone1_phone]" id="phone1"></div>
    <div class="col-xs-12 ">
        <label>Рабочий телефон</label>
        +7<input type="text" value="<?= $contact['phone2'] ?>" name="contact[phone2_phone]" id="phone2"></div>
    <div class="col-xs-12 "><button type="submit" class="btn btn-success save-contact"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Сохранить</button></div>
</form>