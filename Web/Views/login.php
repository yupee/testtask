<div id="login-page">    
    <form action="<?= $data['action']?>" method="POST">
    
    <div class="container">        
        <div class="row">
            <h1 class="col-xs-6 col-xs-offset-3">Вход в аккаунт</h1>
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form-group">
                    <label for="login">Логин</label>
                    <input type="text" name="login" class="login form-control" id="login">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="text" name="password" class="password form-control" id="password">
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3"><button type="submit">Отправить</button></div>
        </div>    
    
    </div>
    </form>
</div>