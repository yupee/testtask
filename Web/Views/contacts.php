<div class="container-fluid">
    <div class="row"><a href="<?= $data['addContactLink'] ?>" class="addContact btn ajax btn-success">Добавить запись</a></div>
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Компания</th>
                        <th>Должность</th>
                        <th>E-mail</th>
                        <th>Рабочий телефон</th>
                        <th>Мобильный Телефон </th>
                        <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr class="filter">
                        <td></td>
                        <td><input type="text" name="name"></td>
                        <td><input type="text" name="surname"></td>
                        <td><input type="text" name="company"></td>
                        <td><input type="text" name="position"></td>
                        <td><input type="text" name="email"></td>
                        <td><input type="text" name="phone1"></td>
                        <td><input type="text" name="phone2"></td>                        
                        <td><a href="<?= $data['filterLink'] ?>"> Отфильтровать</a></td>
                    </tr>
               <?php include(Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/one_contact.php')) ?>;
                </tbody>
            </table>
            <div class="wrapper"></div>
        </div>
    </div>
</div>