<div id="register-page">
    <form action="<?= $data['action']?>" method="POST">
    <div class="container-fluid">    
        <div class="row">
            <div class="col-xs-12">
                <div class="from-group"><label for="name">Ваше Имя</label>
                <input type="text" class="form-control" id="name" name="user[name_string]"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="login">Логин</label>
                    <input type="text" name="user[login_login]" class="validate login form-control" id="login">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="text" name="user[password_password]" class="validate password form-control" id="password">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="email">Ваша почта</label>
                    <input type="email" name="user[email_email]" class="validate email form-control" id="email">
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-xs-12"><button type="submit">Отправить</button></div>
        </div>
    </div>
    </form>
</div>