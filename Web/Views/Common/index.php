<body>

    
    <div class="container">
        <div class="row">
            <nav class="col-xs-12">
                <ul class="nav nav-pills menu">
                    <?php foreach($data['menu'] as $menuItem){
                        echo '<li role="presentation"><a href="' . $menuItem['link'] .'" target="_blank" rel="noopener noreferrer">'. $menuItem['name'] .'</a></li>';
                    } ?>
                </ul>
            </nav>
            <?php if(isset($data['error'])) { ?>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3 ">
                    <p class="bg-danger errors"><?= $data['error'] ?></p>
                </div>
            </div>
            <?php } ?>
        </div>

        <div class="content">
            <?php
                include(Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/' . $data['template']));
            ?>
        </div>

        <?php foreach ($data['scripts'] as $file) { ?>
            <script src=<?= Config::URL_ROOT . $file ?>></script>
        <?php } ?>

    </div>


</body>

</html>