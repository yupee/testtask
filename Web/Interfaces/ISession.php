<?php
interface ISession{
    public function Save($key, $value);

    public function Read($key);

    public function Reset();

}