<?php 
 interface IInputValidator{
     public function Validate($value, $typevalue);
     public function ValidateArray(array $data);
     public function RemoveTypeInfo(array $data);
 }