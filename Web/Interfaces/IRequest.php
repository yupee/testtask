<?php

interface IRequest{
    public function Get( $varName);
    public function Post( $varName);
    public function File($varName);

}