<?php

class InputValidator implements IInputValidator{
    const MIN_PASSWORD_WIDTH = 6;
    const MIN_LOGIN_WIDTH = 4;
    public function Validate($value, $typevalue){
        switch($typevalue){
            case 'login':
                return $this->validateLogin($value);
            case 'password':
                return $this->validatePassword($value);   
            case 'email':
                return $this->validateEmail($value);
            case 'phone':
                return $this->validatePhone($value);
            default:
                return true;
        }

    }

    public function ValidateArray(array $data)
    {
        $result = true;
        foreach($data as $name => $value){
            $nameArr = explode('_', $name);
            if(isset($nameArr[1])){
                $result = ($result === true) ? $this->Validate($value, $nameArr[1]) : $result . $this->Validate($value, $nameArr[1]);
            }
            else{
                continue;
            }
        }        
        return  $result;
    }

    public function RemoveTypeInfo(array $data){
        $result = array();

        foreach($data as $keyTyped =>$value){
            $nameArr = explode('_', $keyTyped);
            $result[$nameArr[0]] = $value;
        }
        return $result;
    }

    private function validatePassword($value){
        $result = true;        
        if(strlen($value) < self::MIN_PASSWORD_WIDTH){
            
            $result = "Пароль должен быть длинее ". self::MIN_PASSWORD_WIDTH ." символов!"."<br>";
        }
        if(!preg_match("#[0-9]+#",$value)) {
            $result .= "Пароль должен содержать хотя бы одну цифру!"."<br>";
        }
        if(!preg_match("#\p{L}#",$value)) {
            $result .= "Пароль должен содержать хотя бы одну букву!"."<br>";
        }
        return $result;

    }
    private function validateLogin($value){
        $result = true;
        if(strlen($value) < self::MIN_LOGIN_WIDTH){
            $result = "Логин должен быть длинее ". self::MIN_LOGIN_WIDTH ." символов!"."<br>";
        }
        if(!preg_match("^[a-zA-Z0-9]*$^",$value)) {
            $result .= "Логин может содержать только латинские буквы и цифры!"."<br>";
        }        
        return $result;
    }

    private function validatePhone($value){
        return (is_numeric( $value)) ? true : 'Неверный формат номера телефона';
    }

    private function validateEmail($value){
        
        return (filter_var($value, FILTER_VALIDATE_EMAIL)) ? true : 'Неверный email! <br>';        
    }
}