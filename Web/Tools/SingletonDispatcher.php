<?php 

class SingletonDispatcher {
    private static $instance;


    public static function GetInstance() : SingletonDispatcher{
        if(self::$instance == null){
            self::$instance = new SingletonDispatcher();
        }
        return self::$instance;

    }

    public function dispatch(IRequest $request, IPhonebookService $phonesrv,
                             IUserService $userSrv, ISession $session, IInputValidator $validator ){
        if($request->Get('route')){
            $route = explode('/', $request->Get('route'));
            $controller = $route[0] . 'Controller';
            $method = (count($route) > 1) ? $route[1] : 'index';           
        }
        else{
            $controller = "accountController";
            $method = "index";

        }        
        if(class_exists($controller) && method_exists($controller, $method)){
            $controllerObj = new $controller($phonesrv, $userSrv, $session, $validator);                            
            if($controllerObj instanceof Controller){                
                $controllerObj->Execute($request, $method);
            }
        }
        else{            
            $controller = 'notfound';
        }

    }

    
}