<?php 

class Request implements IRequest{

    private $post = array();
    private $get = array();
    private $files = array();
    private $sanitizer;
    public function __construct(ISanitizeRequestData $dataCleaner)
    {
        $sanitizer = $dataCleaner;

        foreach($_GET as $getName => $getValue){
            $this->get[$getName] = ($sanitizer == null) ? $getValue : $sanitizer->SanitizeVariable($getValue);
        }
        
        foreach($_POST as $postName => $postValue){
            $this->post[$postName] = ($sanitizer == null) ? $postValue : $sanitizer->SanitizeVariable($postValue);
        }

        foreach($_FILES as $fileName => $file){                        
            $uploadfile =  md5($file['name']) . substr($file['name'],strlen($file['name']) - 4);
            if(move_uploaded_file($file['tmp_name'], Config::STORAGE . $uploadfile)){
                $this->files[$fileName] = Config::STORAGE_URL . $uploadfile;
            }
            else{
                throw new Exception("Файл" . $file['name'] ." не был загружен");
            }
        }
        
    }
    private function findValue($array, $varName){
        if(array_key_exists($varName, $array)){
            return $array[$varName];
        }
        else {
            return false;
        }
    }
    public function Get($varName){

        return $this->findValue($this->get, $varName);

    }
    public function Post($varName){
        return $this->findValue($this->post, $varName);

    }

    public function File($varName){
        return $this->findValue($this->files, $varName);
    }
}

