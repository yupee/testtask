<?php
class SessionStorage implements ISession{
    public function __construct()
    {
        session_start();
    }
    public function Save($key, $value){
        $_SESSION[$key] = $value;
    }

    public function Read($key){
        return (isset($_SESSION[$key])) ? $_SESSION[$key] : null;
    }

    public function Reset(){
        session_reset();
    }

}