<?php 
class PhonebookController extends Controller{
    
    public function index(){

        $phonebooks= $this->phonebookService->PhonebooksByUserId($this->user->GetId(), true);           
        foreach($phonebooks as &$phonebook){            
            $phonebook['deleteLink'] = Config::URL_ROOT . 'index.php?route=Phonebook/RemovePhonebook&phonebook_id=' . (int)$phonebook['id'];
            $phonebook['addContactLink'] = Config::URL_ROOT . 'index.php?route=Phonebook/AddContact&phonebook_id=' . (int)$phonebook['id'];
            foreach($phonebook['contacts'] as &$contact){
                $contact['viewLink'] = Config::URL_ROOT . "index.php?route=phonebook/ViewContact&ajax=1&contact_id=" . (int) $contact['id'] . '&phonebook_id=' . (int)$phonebook['id'];
                $contact['editLink'] = Config::URL_ROOT . "index.php?route=phonebook/EditContact&ajax=1&contact_id=". (int) $contact['id'] . '&phonebook_id=' . (int)$phonebook['id'];
                $contact['removeLink'] = Config::URL_ROOT . "index.php?route=phonebook/removeContact&ajax=1&contact_id=" . (int)$contact['id'] . '&phonebook_id=' . (int)$phonebook['id'];
            }            
        }
        
        $data['phonebooks'] = $phonebooks;
        $data['menu'][] = ['name' => "Выйти", 'link' => Config::URL_ROOT . 'index.php?route=account/logout'];
        $data['template'] = 'phonebook.php';
        $data['addLink'] = Config::URL_ROOT . 'index.php?route=phonebook/AddPhonebook&ajax=1';
        $data['filterLink'] = Config::URL_ROOT . 'index.php?route=phonebook/index&ajax=1';
        $data['contactsPath'] = Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/contacts.php');
        return $data;
     }
     public function AddContact(){
         $data['saveLink'] = Config::URL_ROOT . 'index.php?route=Phonebook/SaveContact&phonebook_id=' .$this->request->Get("phonebook_id");
         $data['contact'] = [
             'name' =>'',
             'surname' => '',
             'company' => '',
             'position'=> '',
             'email'   => '',
             'phone1'  =>'',
             'phone2'  => ''];
         echo(include(Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/addcontact.php')));
         die;

     }

    public function AddPhonebook(){

        $result = $this->validator->Validate($this->request->Post('phonebook[name]'), 'string');        
        
        if($result === true){
            return $this->phonebookService->AddPhonebookForUser($this->user->GetId(), $this->request->Post('phonebook'));
        }
        else{
            return  ['errors'=> [$result]];
        }


    }

    public function ViewContact(){
        $filter = array( ['id' => $this->request->Get('contact_id')]);
        $phonebook = $this->phonebookService->PhonebookById($this->request->Get('phonebook_id'));
        if($this->isUserOwner($phonebook)){
            $contact = $this->phonebookService->ContactsOfPhonebook($this->request->Get("phonebook_id"),$filter);
            $data = $contact[0];            
            echo(include(Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/viewContact.php')));
            die;
        }
        else{
            echo('<p class="bg-danger">Доступ запрещен</p>');
            die;
        }
        
    }

    public function EditPhonebook(){
        $phonebook = $this->phonebookService->PhonebookById($this->request->Get('phonebook_id'));
        if($phonebook  && $this->isUserOwner($phonebook)){
            
            $result = $this->phonebookService->SavePhonebook($phonebook);
            return ['result' => $result];
        }
        else{
            return ['errors' => ['Доступ запрещен']];
        }

    }
    public function EditContact(){
        $phonebook = $this->phonebookService->PhonebookById($this->request->Get('phonebook_id'));
        if($this->isUserOwner($phonebooks)){
            $data['saveLink'] = Config::URL_ROOT . 'index.php?route=Phonebook/SaveContact&phonebook_id=' .$this->request->Get("phonebook_id") .'&contact_id='.$this->request->Get('contact_id');
            $filter = array( ['id' => $this->request->Get('contact_id')]);
            $contact = $this->phonebookService->ContactsOfPhonebook($this->request->Get("phonebook_id"),$filter);        
            $data['contact'] = $contact[0];
            
            echo(include(Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/addcontact.php')));
        }
        else{
            return ['errors' => ['Доступ запрещен']];
        }
        die;


    }    

    public function SaveContact(){                        
        $validation = $this->validator->ValidateArray($this->request->Post('contact'));        
        $phonebook = $this->phonebookService->PhonebookById($this->request->Get('phonebook_id'));        
        
        if($validation === true && $phonebook && $this->isUserOwner($phonebook)){
            $contactData =  $this->validator->RemoveTypeInfo($this->request->Post('contact'));            
            $contactData['image'] = $this->request->File('image');
            $contactData['phonebook_id'] = $this->request->Get('phonebook_id');            
            if($this->request->Get('contact_id')){
                $contactData['id'] = $this->request->Get('contact_id');
            }
            $result = $this->phonebookService->SaveContact($contactData);
            $this->Redirect(['route' => 'phonebook']);
            return ['template' => '<p class="bg-success">Запись сохранена</p>'];

        }
        else {
            return ['errors' => [$validation]];
        }
    }

    private function isUserOwner($phonebook){
        //$phonebook = $this->phonebookService->PhonebookById($this->request->Get('phonebook_id'));
        if($phonebook['user_id'] === $this->user->GetId()){
            return true;
        }
        return false;

    }

    



}