<?php
class Controller{
    protected $needAuth = true;
    protected $userService;
    protected $phonebookService;
    protected $session;
    protected $user;
    protected $validator;

    protected $styles = array();
    protected $scripts = array();
    protected $request;
    

    public function __construct(IPhonebookService $phoneSrv, IUserService $userSrv,
                                ISession $session, IInputValidator $validator)
    {
        $this->userService = $userSrv;
        $this->phonebookService = $phoneSrv;
        $this->session = $session;
        $this->validator = $validator;
    }
    public function Redirect($data){        
        header('Location:' . Config::URL_ROOT . 'index.php?route=' . $data['route']);

    }



    public function Run( $method){        
        $data = $this->$method();
        $this->styles[] = '/Web/css/styles.css';
        $this->scripts[] = 'Web/js/main.js';
        $data['styles'] = $this->styles;
        $data['scripts'] = $this->scripts;
        if(empty($data['menu'])) {
            $data['menu'] = [];
        }
        //ob_start();        
        //dd(scandir('./Views/Common/'));
        include (Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/Common/header.php'));
        include (Config::ConvertToOSPath(Config::ROOT_DIR . '/Web/Views/Common/index.php'));

    }

    public function RunAjax($method){
        
        $data = $this->$method();        
        echo(json_encode($data));
        die;

    }

    public function Execute(IRequest $request, $method){
        
        $this->request = $request;
        if($this->needAuth){            
            $user = $this->session->Read('user');            
            if($user instanceof BaseEntity and $user->GetId() > 0){
                $this->user = $user;
                
            }
            else{
                $this->Redirect(['controller' => 'account']);                
            }
        }        
        if($request->Get('ajax')){
            $this->RunAjax( $method);
        }
        else{
            $this->Run( $method);
        }
        

    }


}