<?php
class accountController extends Controller
{

    protected $needAuth = false;

    public function index()
    {
        $data = ['template' => 'login.php', 'action' => Config::URL_ROOT . 'index.php?route=account/login'];
        $data['menu'] = array(
            ['name' => "Регистрация",
                'link' => Config::URL_ROOT . 'index.php?route=account/register'],
        );
        return $data;
    }

    public function login()
    {

        $login = $this->request->Post('login');
        $password = $this->request->Post('password');
        //$validation = $this->validator->Validate($login,'login') . $this->validator->Validate($password,'password');
        //dd($validation);
        $user = $this->userService->CheckAuth($login, $password);        
        if ($user) {
            $this->session->Save('user', $user);
            
            $this->Redirect(['route' => 'phonebook']);
        }
        $data = ['template' => 'login.php'];
        $data['error'] = 'Пользователя с таким логином и паролем не существует!';

        return $data;
    }

    public function register()
    {
        $errors = '';
        if ($this->request->Post('user')) {
            $validation = $this->validator->ValidateArray($this->request->Post('user'));
            if ($validation === true) {
                $data = $this->validator->RemoveTypeInfo($this->request->Post('user'));
                $result = $this->userService->RegisterNew($data);                

                if ($result) {
                    return ['template' => 'success.php'];
                } else {
                    $errors .= 'Возникла нпредвиденная ошибка';
                }
            }
            $errors .= $validation;

        }

        $menu = array(['name' => 'Войти', 'link' => Config::URL_ROOT . 'index.php?route=account']);
        return ['template' => 'register.php',
            'menu' => $menu,
            'error' => $errors,
            'action' => Config::URL_ROOT . 'index.php?route=account/register'];

    }

    public function logout()
    {
        session_reset();
    }
}
