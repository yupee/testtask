
$(function(){

    $('#createPhonebook').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('.modal-title').html("Создать новую телефонную книгу");
        $('.modal-body').html('<label for="name">Название</label><input name="phonebook[name]" type="text"/>');

        $('#savebtn').click(function(e){
            $.ajax({
                type: "POST",
                url: url,
                data: $('#modal-wnd input'),

                success: function(data){
                    if(data['error']){
                        $('.modal-body').html('<p class="bg-danger">'+ data['error'] +'</p>');
                    }
                    else{                    
                        $('.modal-body').html('<p class="bg-success">Телефонная книга создана успешно!</p>');
                        window.setTimeout(function(){
                            $('#modal-wnd').modal('hide');
                        }, 5000);
                    }
                },
                dataType: 'json',
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
        });
        $('#modal-wnd').modal('show');

    });
    $('.filter a').change(function(){
        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),                        
            dataType: 'json',
            data:$(this).closest('.filter').find('input'),
            success:function(data){                                    
               console.log(data);
            },
    
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          }); 
    });
    $('.viewContact').click(function(e){
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),                        
            dataType: 'html',
            data:$(this).closest('.table-responsive').find('input'),
            success:function(html){                                    
                $('.modal-body').html(html);   
                $('#modal-wnd').modal('show');
            },
    
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });     
    });

    $('.addContact, .editContact').click(function(e){
        e.preventDefault();
        $(this).closest('.table-responsive').find('.wrapper').addClass('pastHere');

        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),                        
            dataType: 'html',
            data:$(this).closest('.table-responsive').find('input'),
            success:function(html){
                $('.pastHere').removeClass("pastHere").html(html);
            },
    
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });           
    });

    /* $(document).on('submit', '.add-contact-form', function(e){
        e.preventDefault();
        $(this).closest('.table-responsive').find('.wrapper').addClass('pastHere');
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),                        
            dataType: 'json',
            data:$(this).serialize(),
            success:function(data){
                if(data['error']){
                    alert(data['error']);
                }
                else{
                    $('.pastHere').removeClass("pastHere").html(data['html']);
                }
            },
    
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });        
    }) */
    
});
