<?php 
class Autoloader {
    protected static $classIndex = array();

    public static function load($class_name){

        $class = strtolower($class_name);        
        if(array_key_exists($class, self::$classIndex)){
            require_once(self::$classIndex[$class]);
        }
    }
    public static function init($dir){
        $dirCnt = scandir($dir);    
        foreach($dirCnt as $oneNode){
            if($oneNode == '.' | $oneNode == '..') continue;

            $forCheck = $dir . '/' . $oneNode;
            if(is_dir($forCheck)){
                self::init($forCheck, self::$classIndex);
            }
            else 
            {
                $path = explode('/',substr($forCheck, 0, strlen($forCheck) - 4));
                $className = $path[count($path)-1];
                
                if(substr($forCheck, strlen($forCheck) - 4, 4) == '.php' ) 
                {
                    self::$classIndex[strtolower($className)] = $forCheck;
                    
                }
            }
        }
    }
}