<?php
class UserService extends Service implements IUserService
{

    public function CheckAuth($login, $password)
    {
        $criterias = ['login' => $login];
        $user = $this->rep->FindOne($criterias);
        return ($user != null && $user->Password() == md5($password)) ? $user : null;
    }
    public function RegisterNew($userData)
    {
        $user = $this->factory->CreateEntity('User', $userData);
        if ($this->CheckExist($user->login(), $user->email())) {
            return ['error' => "Логин или E-mail уже зарегестрированы в системе"];
        }
        return $this->rep->SaveEntity($user);
    }

    protected function CheckExist($login, $email)
    {
        $criterias = [
            ['login' => $login, 'email' => $email, 'method' => Config::CRITERIA_METHOD_OR]
        ];        
        $user = $this->rep->FindOne($criterias);
        return ($user) ? true : false;
    }

    public function UpdateUserData($userData)
    {
        $user = $this->rep->GetEntityById($userData['id']);
        $emailSearch = $this->rep->FindOne(['email' => $userData['email']]);
        if ($user->GetId() == $emailSearch->GetId()) {

            $user->SetData($userData);
            return $this->rep->SaveEntity($user);
        }
        else{
            return ['error' => "Пользователь с таким Email уже зарегестрирован в системе"];
        }
    }

    public function IsLogged(){
        
    }
}
