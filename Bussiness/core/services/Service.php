<?php

abstract class Service{
    protected $rep;
    protected $factory;

    public function __construct(IRepository $repository, IFactory $factory)
    {
        $this->rep = $repository;
        $this->factory = $factory;
    }
}