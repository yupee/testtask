<?php 

class PhonebookService extends Service implements IPhonebookService{
    

    public function PhonebooksByUserId($userId, bool $withContacts = false){
        $criterias = ['user_id' => $userId];
        $result = array();
        $phonebookObjs = $this->rep->Select($criterias);        
        foreach($phonebookObjs as $book){
            $phonebookData = $book->GetEntityData();
            if($withContacts){
                $contacts = $this->ContactsEntityForPhonebook($book->GetId());
                $book->SetContacts($contacts);
                $phonebookData['contacts'] = $book->Contacts();
            }
            $result[] = $phonebookData;
        }


        return $result;
    }

    public function PhonebookById($phonebookId, bool $withContacts = false){
        $book = $this->rep->GetEntityById($phonebookId);
        if($withContacts){
            $contacts = $this->ContactsOfPhonebook($phonebookId);
            $book->SetContacts($contacts);
        }
        return $book->GetEntityData();

    }

    private function ContactsEntityForPhonebook($phonebookId, array $filterData = array(), array $sortData = array()){
        $filterData[] = ['phonebook_id' => $phonebookId];
        $filterData['method'] = Config::CRITERIA_METHOD_AND;
        $criterias = $filterData;
        $entities = $this->rep->Select($criterias,'Contact');
        return $entities;
    }

    public function ContactsOfPhonebook($phonebookId, array $filterData = array(), array $sortData = array()){

        $entities=$this->ContactsEntityForPhonebook($phonebookId, $filterData, $sortData);
        $result = array();
        foreach($entities as $obj){
            $result[] = $obj->GetEntityData();
        }
        return $result;

    }

    public function AddPhonebookForUser($userId, array $phonebookData){
        $phonebookData['user_id'] = $userId;
        
        $phonebook = $this->factory->CreateEntity('Phonebook', $phonebookData);
        return $this->rep->SaveEntity($phonebook);        
    }

    public function SavePhonebook($phonebookData){
        unset($phonebookData['contacts']);        
        $phonebook = $this->factory->CreateEntity('Phonebook',$phonebookData);
        return $this->rep->SaveEntity($phonebook);
    }    

    public function SaveContact($contactData){
        $contact = $this->factory->CreateEntity('Contact',$contactData);
        return $this->rep->SaveEntity($contact);
    }


}