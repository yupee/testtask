<?php

abstract class  BaseEntity {
    protected $id = 0;

    protected $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function GetId(){
        return $this->id;

    }
    public function Type(){
        return $this->type;
    }

    abstract public  function GetEntityData() : array;
    abstract public function SetEntityData(array $data);
}