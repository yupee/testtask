<?php

class Repository implements IRepository
{
    private $storage;
    private $entityType;
    private $factory;

    public function __construct(IStorage $db, string $entityType, IFactory $factory)
    {
        $this->storage = $db;
        $this->entityType = $entityType;
        $this->factory = $factory;
    }

    public function GetEntityById($id): BaseEntity
    {
        $entityData = $this->storage->Read($this->entityType, $id);
        $entity = $this->factory->CreateEntity($this->entityType, $entityData);
        return $entity;
    }

    public function SaveEntity(BaseEntity $entity)
    {
        $entityData = $entity->GetEntityData();
        $entityType = $entity->Type();
        if ($entity->GetId() > 0) {
            return $this->storage->Update($entityType, $entityData, ['id' => $entity->GetId()]);
        } else {
            return $this->storage->Create($entityType, $entityData);
        }

    }

    public function RemoveEntity($id)
    {
        $this->storage->Delete($this->entityType, $id);

    }

    public function FindOne(array $criterias)
    {
        $data = $this->storage->Select($this->entityType, $criterias, 1);
        return ($data) ? $this->factory->CreateEntity($this->entityType, $data) : null;
    }

    // entity type need only for contacts, it is better to create special PhonebiookRepository if logic getting more complex
    public function Select(array $criterias, $entityType = null, array $sort = null, $limit = null)
    {
        $entityType = ($entityType == null) ? $this->entityType : $entityType;        
        $data = $this->storage->Select($entityType, $criterias);
        $entities = array();        
        if ($data != null) {
            foreach ($data as $entityData) {
                $entity = $this->factory->CreateEntity($entityType, $entityData);
                
                    $entities[] = $entity;
                
            }
        }
        return $entities;

    }

}
