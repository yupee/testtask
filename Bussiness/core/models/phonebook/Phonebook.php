<?php 

class Phonebook extends BaseEntity implements IPhonebook{
    private $name;
    private $userId;
    private $contacts = array();

    public function Name(){
        return $this->name;
    }

    public function UserId(){
        return $this->userId;
    }

    public function Contacts() : array{
        $result = array();

        foreach($this->contacts as $contact){
            $result[] = $contact->GetEntityData();
        }
        return $result;
    }
    
    public function SetContacts(array $contacts) { 
        
        foreach($contacts as $contact)       {
            if($contact instanceof IContact){
                $this->contacts[] = $contact;
            }
            else{
                throw new Exception("Переменная не реализует интерфейс IContact");
            }
        }        
    }

    public function AddContact(IContact $contact){
        $this->contacts[] = $contact;

    }

    public function GetEntityData() : array{        
        $result =  [
            'name' => $this->name,
            'user_id' => $this->userId];
        if($this->id > 0){
            $result['id'] = $this->id;
        }
        /* if(count($this->contacts) > 0){
            $result['contacts'] = $this->Contacts();
        }  */
        return $result;

    }
    public function SetEntityData(array $data){
        $result = true;
        if(isset($data['name'])){
            $this->name = $data['name'];
        }
        else{
            $result = 'Ошибка, имя не может быть пустым';
        }
        if($data['id'] > 0){
            $this->id = $data['id'];
        }
        if(isset($data['user_id'])){
            $this->userId = $data['user_id'];
        }        
        else{
            $result = 'Ошибка, Телефонная книга должна быть привязана к пользователю';
        }
        return $result;
    }

    

}