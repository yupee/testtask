<?php 

class Contact extends BaseEntity implements IContact{
    protected $name;
    protected $email;
    protected $image;
    protected $surname;
    protected $company;
    protected $position;
    protected $phonebookId;
    protected $phone1;
    protected $phone2;

    public function Name(){
        return $this->name;
    }
    public function Email(){
        return $this->email;
    }
    public function Image(){
        return $this->image;
    }
    public function Surname(){
        return $this->surname;
    }
    public function Company(){
        return $this->company;
    }
    public function Phone1(){
        return $this->phone1;
    }
    public function Phone2(){
        return $this->phone2;
    }
    public function PhonebookId(){
        return $this->phonebookId;
    }

    public function SetEntityData(array $data){        
        $result = true;
        if(isset($data['id'])){
            $this->id = $data['id'];
        }
        if(isset($data['name'])){
            $this->name = $data['name'];
        }
        else{
            $result = 'Ошибка, имя не может быть пустым';
        }
        if(isset($data['email'])){
            $this->email = $data['email'];
        }
        if(isset($data['position'])){
            $this->position = $data['position'];
        }
        if(isset($data['surname'])){
            $this->surname = $data['surname'];
        }
        if(isset($data['image'])){
            $this->image = $data['image'];
        }
        if(isset($data['company'])){
            $this->company = $data['company'];
        }
        if(isset($data['phone1'])){
            $this->phone1 = $data['phone1'];
        }
        
        else{
            $result = 'Ошибка, телефон не может быть пустым';
        }
        if(isset($data['phonebook_id'])){
            $this->phonebookId = $data['phonebook_id'];
        }
        else{
            $result = 'Ошибка, контакт должен относиться к телефонной книге';
        }
        if(isset($data['phone2'])){
            $this->phone2 = $data['phone2'];
        }
        return $result;
    }
    public function GetEntityData() : array {
        $result = [
                'name' => $this->name,
                'phonebook_id' => $this->phonebookId,
                'email' => $this->email,
                'surname' => $this->surname,
                'company' => $this->company,
                'phone1'   => $this->phone1,
                'phone2'   => $this->phone2,
                'image'    => $this->image,
                'position' => $this->position
            ];
            
            if($this->id > 0){
                $result['id'] = $this->id;
           }
        return $result;

    }
}