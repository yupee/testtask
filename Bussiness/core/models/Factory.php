<?php 
class Factory implements IFactory{

    public function CreateEntity($type, $data) : BaseEntity {
        if(class_exists($type)){
            
            $entity = new $type($type);
            $entity->SetEntityData($data);
            return $entity;
        }
        else{
            throw new Exception(" Класс не существует $type");
        }
    }
}