<?php 
class User extends BaseEntity implements IUser{

    private $login;
    private $password;
    private $email;
    private $name;

    public function login(){
        return $this->login;
    }

    public function Password(){
        return $this->password;
    }

    public function Email(){
        return $this->email;
    }
    public function Name(){
        return $this->name;
    }
    public function GetEntityData() : array {
        $result = [
            'name' => $this->name,                
            'email' => $this->email,            
            'password'    => md5($this->password),
            'login'    => $this->login
        ];
        if($this->id > 0){
            $result['id'] = $this->id;
    }
        return $result;

    }
    public function SetEntityData(array $data){
        $result = true;
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        if(isset($data['login'])){
            $this->login = $data['login'];
        }
        else{
            $result = 'Ошибка, логин не может быть пустым';
        }
        if(isset($data['password'])){
            $this->password = $data['password'];
        }
        else{
            $result = 'Ошибка, пароль не может быть пустым';
        }
        if(isset($data['email'])){
            $this->email = $data['email'];
        }
        if(isset($data['name'])){
            $this->name = $data['name'];
        }
        return $result;

    }
}