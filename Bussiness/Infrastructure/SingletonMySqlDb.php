<?php

class SingletonMySqlDb implements IStorage{

    private static $instance;
    const  TYPE_TABLE_MAP = ['User' => 'users',
                            'Phonebook' => 'phonebook',
                            'Contact' => 'contacts'];
    

    private $pdo;

    protected static function MapCriteriaMethod($method){
        switch ($method) {
            case Config::CRITERIA_METHOD_AND:
                return ' AND ';
            case Config::CRITERIA_METHOD_OR:
                return ' OR ';
        }
    }

    private function __construct(){
        $dsn = $dsn = "mysql:host=". Config::DB_HOST .";dbname=". Config::DB_NAME .";charset=utf8";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try{
            $this->pdo = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD, $opt); 
        }
        catch (PDOException $e) {            
            throw $e;
        }
    }

    public function GetInstance(){
        if(self::$instance  == null){
            self::$instance = new SingletonMySqlDb();
        }
        return self::$instance;
    }

    public function Read($type, $id){
        $sql = "SELECT * FROM " . self::TYPE_TABLE_MAP[$type] . " t WHERE `id` = ?";

        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));
        return $query->fetch();

    }
    public function Create($type, array $data){        
        $sql = "INSERT INTO " . self::TYPE_TABLE_MAP[$type] . " ( ";
        $values = " VALUES (";
        foreach($data as $columnName => $value){
            $sql .= $columnName . ",";
            $values .= ":" . $columnName . ",";

        }
        
        $sql = rtrim($sql, ',');
        $values = rtrim($values, ',');        
        $sql = $sql . ') ' . $values . ') ;';        
        

        $query = $this->pdo->prepare($sql);
        foreach($data as $columnName => $value){
            $query->bindValue(":$columnName", $value);        
        }                
        return $query->execute();


    }
    public function Update($type, array $data, array $conditions){
        if(count($conditions) == 0){
            throw new Exception('Для обновления записи необходимо условие');
        }
        $sql = "UPDATE " . self::TYPE_TABLE_MAP[$type] . ' SET ';        
        foreach($data as $columnName => $value){
            if($columnName == 'id') continue;
            $sql .= ' `' . $columnName . "` = :data" . $columnName . ',' ;            
        }
        $sql = rtrim($sql, ',');
        

        
        $conditionData = $this->PrepareWhere($conditions);
        $sql = ($conditionData['sql'] == '') ? $sql : $sql . ' WHERE ' . $conditionData['sql'];        
        //dd($sql);
        $query = $this->pdo->prepare($sql);
        foreach($data as $columnName => $value){
            if($columnName == 'id') continue;
            $query->bindValue(":data$columnName", $value);
        }
        foreach($conditionData['columnValues'] as $columnName => $value){
            $query->bindValue(":$columnName", $value);
        }
        
        return $query->execute();

    }
    public function Delete($type, array $condition){

    }


    public function Select($type, array $conditions, $limit = null){
        $sql = 'SELECT * FROM '. self::TYPE_TABLE_MAP[$type] . ' t ';
        $columnValues = [];        
        $conditionData = $this->PrepareWhere($conditions, $columnValues, 't');        
        if($conditionData){
            $columnValues = $conditionData['columnValues'];
            $sql .= ' WHERE ' . $conditionData['sql'];            
        }        
        
        if($limit){
            $sql .= " LIMIT $limit";
        }                                
        $query = $this->pdo->prepare($sql);
        
        foreach($columnValues as $col => $val){
            $query->bindValue(":$col", $val);
        }        
        $query->execute();                
        return ($limit == 1) ? $query->fetch() : $query->fetchAll();

    }

    private function PrepareWhere($conditions, &$columnValues = array(), $tableAlias=''){
        $sql = '';        
        if($tableAlias != ''){
            $tableAlias .= '.';
        }
        if($conditions == null | count($conditions) == 0){
            return null;
        }

        if(count($conditions) == 1){

            if(isset($conditions[0])){
                $method = $this->MapCriteriaMethod($conditions[0]['method']);
                unset($conditions[0]['method']);
                $map = $conditions[0];
            }
            else{
                $map = $conditions;
                $method = '';
            }

            foreach($map as $col => $val){
                $sql .= "$tableAlias`".$col . "`= :$col $method";
                $columnValues[$col] = $val;
            }
            $sql = substr($sql, 0, strlen($sql) - strlen($method));
            
            //$sql = ($sql != '' && !strpos($sql, 'WHERE')) ? "WHERE " .$sql : $sql;
        }
        else{
            $conditionMethod = '';            

            //d($conditions);
            if(isset($conditions['method']) and count($conditions) > 1){
                $conditionMethod = $this->MapCriteriaMethod($conditions['method']);
                unset($conditions['method']);
            }

            foreach($conditions as $oneCriteria){                
                unset($oneCriteria['method']);
                $oneCriteriaCondition = $this->PrepareWhere($oneCriteria, $columnValues);
                $sql .= ' (' . $oneCriteriaCondition['sql'] . ') ' . $conditionMethod;
                

            }

            if($conditionMethod){
                $sql = substr($sql, 0, strlen($sql) - strlen($conditionMethod));
            }
            //$sql = 'WHERE ' . $sql;
        }
                
        return ['sql' => $sql, 'columnValues' => $columnValues];

    }


}