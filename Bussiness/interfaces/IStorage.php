<?php
interface IStorage{    

    /* public function User($userId);
    public function Phonebook($phonebookId);
    public function PhonebookAll(array $criterias, array $sort);
    public function UserByLogin($login);
    public function ContactsOfPhonebook($phonebookId);
    public function ContactDetails($contact_id); */

    public function Read(string $type, int $id);
    public function Create(string $type, array $data);
    public function Update(string $type, array $data, array $condition);
    public function Delete(string $type, array $condition);


    public function Select(string $type, array $conditions, $limit = null);
    //public function SelectJoin(array $types, array $condition);

    
    
}