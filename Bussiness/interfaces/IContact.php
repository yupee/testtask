<?php
interface IContact{
    public function Name();
    public function Email();
    public function Image();
    public function Surname();
    public function Company();
    public function Phone1();
    public function Phone2();
}