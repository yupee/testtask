<?php
interface IPhonebook{
    public function Name();

    public function UserId();

    public function Contacts() : array;

    public function AddContact(IContact $contact);

    public function SetContacts(array $contacts);

}