<?php 
interface IUserService{

    public function CheckAuth($login, $password);
    public function RegisterNew($userData);
    public function IsLogged();

}