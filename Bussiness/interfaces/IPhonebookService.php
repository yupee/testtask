<?php 

interface IPhonebookService{
    public function PhonebooksByUserId($userId, bool $withContacts = false);
    public function PhonebookById($phonebookId, bool $withContacts = false);
    public function ContactsOfPhonebook($phonebookId, array $filterData, array $sortData);    
    public function SavePhonebook($phonebookData);
    public function SaveContact($contactData);
}