<?php

interface IRepository{
    public function GetEntityById($id) : BaseEntity;

    public function SaveEntity(BaseEntity $entity);

    public function RemoveEntity($id);

    public function FindOne(array $criterias);

    public function Select(array $criterias,$entityType, array $sort,  $limit);

    

}