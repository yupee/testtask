<?php 
class Config{
    const ROOT_DIR = __DIR__;
    const CRITERIA_METHOD_OR = 1;
    const CRITERIA_METHOD_AND = 0;   
    
    const DB_HOST = 'localhost';
    const DB_NAME = 'testtask';
    const DB_USER = 'root';
    const DB_PASSWORD = '';

    const DIR_SEPARATOR = '\\';
    const URL_ROOT = 'http://localhost/whynot/';
    const STORAGE = self::ROOT_DIR . self::DIR_SEPARATOR . 'storage'. self::DIR_SEPARATOR;
    const STORAGE_URL = self::URL_ROOT . self::DIR_SEPARATOR . 'storage'. self::DIR_SEPARATOR;

    public static function  ConvertToOSPath($path){
        return str_replace('/', self::DIR_SEPARATOR, $path);
    }

}

function d($var,$caller=null)
{
    if(!isset($caller)){
        $tempArray = debug_backtrace(1);
        $caller = array_shift($tempArray);
    }
    $arr_debug = debug_backtrace();
    print_r('file:' . $arr_debug[0]['file']. ' line: '. $arr_debug[0]['line']);
    echo '<xmp style="text-align: left;">';
    print_r($var);
    echo '</xmp><br />';
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var)
{
    if(!isset($caller)){
        $tempArray = debug_backtrace(1);
        $caller = array_shift($tempArray);
    }
    $arr_debug = debug_backtrace();
    print_r('file:' . $arr_debug[0]['file']. ' line: '. $arr_debug[0]['line']);
    echo '<xmp style="text-align: left;">';
    print_r($var);
    echo '</xmp><br />';
    die();
}